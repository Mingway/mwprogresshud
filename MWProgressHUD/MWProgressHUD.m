//
//  MWProgressHUD.m
//  MWProgressHUD
//
//  Created by shimingwei on 14-9-5.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MWProgressHUD.h"

static const float kProgressHUDWidth = 150.0f;
static const float kProgressHUDHeight = 150.0f;

@interface MWBgWindow : UIWindow

+ (instancetype)defaultWindow;

@end

@implementation MWBgWindow

+ (instancetype)defaultWindow
{
    static MWBgWindow *window = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        window = [[MWBgWindow alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    });
    return window;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.5];
    }
    return self;
}

@end

#pragma mark -
#pragma mark MWProgressHUD

@interface MWProgressHUD (){
    UIWindow *_keyWindow;
    MWStyle _type;
    float _delayDuration;
    BOOL _isSetDealyDuration;
    
    UIActivityIndicatorView *_activityIndicatorView;
    UIButton *_cancelBtn;
    void(^_cancelBlock)(void);
    
    UIImageView *_imageView;
    UILabel *_msgLabel;
}

@end

@implementation MWProgressHUD

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithType:(MWStyle)type
{
    self = [super initWithFrame:CGRectMake(0, 0, kProgressHUDWidth, kProgressHUDHeight)];
    if (self) {
        
        _delayDuration = 3.0f;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarFrameNotification
                                                   object:nil];
        
        _type = type;
        
        switch (type) {
            case MWStyleWait:{
                _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];
                _activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
                [self addSubview:_activityIndicatorView];
                _activityIndicatorView.center = CGPointMake(kProgressHUDWidth/2, kProgressHUDHeight/2 - 10);
                
                _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                [_cancelBtn setFrame:CGRectMake(0, kProgressHUDHeight - 50, kProgressHUDWidth, 50)];
                [_cancelBtn setTitle:@"cancel"
                            forState:UIControlStateNormal];
                [_cancelBtn setTitleColor:[UIColor whiteColor]
                                 forState:UIControlStateNormal];
                [_cancelBtn addTarget:self
                               action:@selector(dismiss)
                     forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:_cancelBtn];
            }
                break;
            case MWStyleInfo:{
                [self initImageViewAndMsgLabelWithImageName:@"info"];
            }
                break;
            case MWStyleSuccess:{
                [self initImageViewAndMsgLabelWithImageName:@"success"];
            }
                break;
            case MWStyleFail:{
                [self initImageViewAndMsgLabelWithImageName:@"fail"];
            }
                break;
            default:
                break;
        }
        
        self.backgroundColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.8];
        self.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.9].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 10;
    }
    return self;
}

- (void)initImageViewAndMsgLabelWithImageName:(NSString*)imageName
{
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake((kProgressHUDWidth - 40)/2, 40, 40, 40)];
    _imageView.image = [UIImage imageNamed:imageName];
    [self addSubview:_imageView];
    
    _msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_imageView.frame) + 30, kProgressHUDWidth, 20)];
    _msgLabel.textColor = [UIColor whiteColor];
    _msgLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_msgLabel];
    _isSetDealyDuration = YES;
}

- (void)setCancelBlock:(void (^)(void))willDoMethod
{
    _cancelBlock = willDoMethod;
}

- (void)setDismissDelayDuration:(float)delayDuration
{
    if (delayDuration > 0) {
        _delayDuration = delayDuration;
        _isSetDealyDuration = YES;
    }
}

- (void)setMsg:(NSString*)msg
{
    _msgLabel.text = msg;
}

- (void)show
{
    switch (_type) {
        case MWStyleWait:{
            [_activityIndicatorView startAnimating];
        }
            break;
        case MWStyleInfo:{
            
        }
            break;
        case MWStyleSuccess:{
            
        }
            break;
        case MWStyleFail:{
            
        }
            break;
        default:
            break;
    }
    
    _keyWindow = [UIApplication sharedApplication].keyWindow;
    [self rotateAccordingToStatusBarOrientationAndSupportedOrientations];
    
    for (UIView *subview in [MWBgWindow defaultWindow].subviews) {
        [subview removeFromSuperview];
    }
    [[MWBgWindow defaultWindow] addSubview:self];
    [[MWBgWindow defaultWindow] makeKeyAndVisible];
    
    if (_isSetDealyDuration) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(dismiss)
                                                   object:nil];
        [self performSelector:@selector(dismiss)
                   withObject:nil
                   afterDelay:_delayDuration];
    }
}

- (void)dismiss
{
    if (_isSetDealyDuration) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(dismiss)
                                                   object:nil];
    }
    
    [[MWBgWindow defaultWindow] resignKeyWindow];
    [[MWBgWindow defaultWindow] setHidden:YES];
    [_keyWindow makeKeyAndVisible];
    [self removeFromSuperview];
    
    switch (_type) {
        case MWStyleWait:{
            [_activityIndicatorView stopAnimating];
            _cancelBlock();
        }
            break;
        case MWStyleInfo:{
            
        }
            break;
        case MWStyleSuccess:{
            
        }
            break;
        case MWStyleFail:{
            
        }
            break;
        default:
            break;
    }
}

//Orientation (statusBar)
- (void)statusBarFrameOrOrientationChanged:(NSNotification *)notification
{
    [self rotateAccordingToStatusBarOrientationAndSupportedOrientations];
}

- (void)rotateAccordingToStatusBarOrientationAndSupportedOrientations
{
    UIInterfaceOrientation statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
    CGFloat angle = UIInterfaceOrientationAngleOfOrientation(statusBarOrientation);
    CGFloat statusBarHeight = [[self class] getStatusBarHeight];
    
    CGAffineTransform transform = CGAffineTransformMakeRotation(angle);
    CGRect frame = [[self class] rectInWindowBounds:[UIApplication sharedApplication].keyWindow.bounds statusBarOrientation:statusBarOrientation statusBarHeight:statusBarHeight];
    
    [self setIfNotEqualTransform:transform frame:frame];
}

- (void)setIfNotEqualTransform:(CGAffineTransform)transform frame:(CGRect)frame
{
    if(!CGAffineTransformEqualToTransform(self.transform, transform))
    {
        self.transform = transform;
    }
    if(!CGRectEqualToRect(self.frame, frame))
    {
        self.frame = frame;
    }
}

+ (CGFloat)getStatusBarHeight
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(UIInterfaceOrientationIsLandscape(orientation))
    {
        return [UIApplication sharedApplication].statusBarFrame.size.width;
    }
    else
    {
        return [UIApplication sharedApplication].statusBarFrame.size.height;
    }
}

+ (CGRect)rectInWindowBounds:(CGRect)windowBounds statusBarOrientation:(UIInterfaceOrientation)statusBarOrientation statusBarHeight:(CGFloat)statusBarHeight
{
    CGRect frame;
    if (statusBarOrientation == UIDeviceOrientationPortrait || statusBarOrientation == UIDeviceOrientationPortraitUpsideDown) {
        frame = CGRectMake(CGRectGetWidth(windowBounds)/2 - kProgressHUDWidth/2, CGRectGetHeight(windowBounds)/2 - kProgressHUDHeight/2, kProgressHUDWidth, kProgressHUDHeight);
    }else{
        frame = CGRectMake(CGRectGetWidth(windowBounds)/2 - kProgressHUDHeight/2, CGRectGetHeight(windowBounds)/2 - kProgressHUDWidth/2, kProgressHUDHeight, kProgressHUDWidth);
    }
    return frame;
}

CGFloat UIInterfaceOrientationAngleOfOrientation(UIInterfaceOrientation orientation)
{
    CGFloat angle;
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortraitUpsideDown:
            angle = M_PI;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            angle = -M_PI_2;
            break;
        case UIInterfaceOrientationLandscapeRight:
            angle = M_PI_2;
            break;
        default:
            angle = 0.0;
            break;
    }
    
    return angle;
}

UIInterfaceOrientationMask UIInterfaceOrientationMaskFromOrientation(UIInterfaceOrientation orientation)
{
    return 1 << orientation;
}

@end
