//
//  MWViewController.m
//  MWProgressHUD
//
//  Created by shimingwei on 14-9-5.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MWViewController.h"
#import "MWProgressHUD.h"

@interface MWViewController ()

- (IBAction)show:(UIButton *)sender;
- (IBAction)info:(UIButton *)sender;
- (IBAction)success:(UIButton *)sender;
- (IBAction)fail:(UIButton *)sender;

@end

@implementation MWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)show:(UIButton *)sender {
    MWProgressHUD *progressHUD = [[MWProgressHUD alloc] initWithType:MWStyleWait];
    
    __block MWProgressHUD *tmpProgressHUD = progressHUD;
    [progressHUD setCancelBlock:^{
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getDataSuccess:) object:tmpProgressHUD];
        NSLog(@"cancel request");
    }];
    [progressHUD show];
    
    [self performSelector:@selector(getDataSuccess:) withObject:progressHUD afterDelay:3.0f];
}

- (IBAction)info:(UIButton *)sender {
    MWProgressHUD *progressHUD = [[MWProgressHUD alloc] initWithType:MWStyleInfo];
    [progressHUD setDismissDelayDuration:3.0f];
    [progressHUD setMsg:@"详细信息"];
    [progressHUD show];
}

- (IBAction)success:(UIButton *)sender {
    MWProgressHUD *progressHUD = [[MWProgressHUD alloc] initWithType:MWStyleSuccess];
    [progressHUD setDismissDelayDuration:3.0f];
    [progressHUD setMsg:@"成功"];
    [progressHUD show];
}

- (IBAction)fail:(UIButton *)sender {
    MWProgressHUD *progressHUD = [[MWProgressHUD alloc] initWithType:MWStyleFail];
    [progressHUD setDismissDelayDuration:3.0f];
    [progressHUD setMsg:@"失败"];
    [progressHUD show];
}

- (void)getDataSuccess:(MWProgressHUD*)hud
{
    [hud dismiss];
    MWProgressHUD *progressHUD = [[MWProgressHUD alloc] initWithType:MWStyleSuccess];
    [progressHUD setDismissDelayDuration:3.0f];
    [progressHUD setMsg:@"成功"];
    [progressHUD show];
}

@end
