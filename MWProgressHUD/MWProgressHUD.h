//
//  MWProgressHUD.h
//  MWProgressHUD
//
//  Created by shimingwei on 14-9-5.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MWStyle) {
    MWStyleWait = 1,
    MWStyleInfo,
    MWStyleSuccess,
    MWStyleFail
};

@interface MWProgressHUD : UIView

- (instancetype)initWithType:(MWStyle)type;
//如需设置自动消失时间，可以设置该参数
- (void)setDismissDelayDuration:(float)delayDuration;
//如需在徐小猴调用某些方法，可以设置如下block
- (void)setCancelBlock:(void(^)(void))willDoMethod;
//设置显示文本
- (void)setMsg:(NSString*)msg;
- (void)show;
- (void)dismiss;

@end
