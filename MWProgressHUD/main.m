//
//  main.m
//  MWProgressHUD
//
//  Created by shimingwei on 14-9-5.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MWAppDelegate class]));
    }
}
